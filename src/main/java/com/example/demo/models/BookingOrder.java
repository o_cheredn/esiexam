package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Oleksandr on 6/9/2017.
 */

@Entity
@Data
public class BookingOrder {
    @Id
    String id;

    @Enumerated (EnumType.STRING)
    BookingOrderStatus status;

    @ManyToOne
    BookingRequest request;

    @ManyToOne
    PropertyItem property;

    //*@OneToMany
    //*List<BookingRequest> reservations;

}

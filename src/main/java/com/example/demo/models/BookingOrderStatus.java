package com.example.demo.models;

/**
 * Created by Oleksandr on 6/9/2017.
 */
public enum BookingOrderStatus {
    ACCEPTED, CANCELLED, CLOSED
}

package com.example.demo.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

/**
 * Created by Oleksandr on 6/9/2017.
 */

@Entity
@Data
public class BookingRequest {
    @javax.persistence.Id
    String id;


    BigDecimal bitcoins;

    BigDecimal rentamount;

    @Embedded
    RentalPeriod rperiod;

    @ManyToOne
    Guest guest;

    @ManyToOne
    PropertyItem item;

    @ManyToOne
    BookingOrder order;
}

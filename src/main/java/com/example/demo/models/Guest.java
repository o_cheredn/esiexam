package com.example.demo.models;

import com.sun.javafx.beans.IDProperty;
import lombok.Data;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by Oleksandr on 6/9/2017.
 */

@Entity
@Data
public class Guest {
    @Id
    String id;

    String details;
    String preferences;

    BigDecimal balance;


    @Embedded
    BitcoinDetails bdetails;
}

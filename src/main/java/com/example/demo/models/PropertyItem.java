package com.example.demo.models;

import lombok.Data;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by Oleksandr on 6/9/2017.
 */
@Entity
@Data

public class PropertyItem {
    @Id
    String id;

    BigDecimal price;
    String city;
    String address;
    BigDecimal area;
    Integer floor;
    String amenities;
    String services;

    @Embedded
    RentalPeriod period;

}

insert into plant_inventory_entry (id, name, description, price) values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150);
insert into plant_inventory_entry (id, name, description, price) values (2, 'Mini excavator', '3 Tonne Mini excavator', 200);
insert into plant_inventory_entry (id, name, description, price) values (3, 'Midi excavator', '5 Tonne Midi excavator', 250);
insert into plant_inventory_entry (id, name, description, price) values (4, 'Midi excavator', '8 Tonne Midi excavator', 300);
insert into plant_inventory_entry (id, name, description, price) values (5, 'Maxi excavator', '15 Tonne Large excavator', 400);
insert into plant_inventory_entry (id, name, description, price) values (6, 'Maxi excavator', '20 Tonne Large excavator', 450);
insert into plant_inventory_entry (id, name, description, price) values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150);
insert into plant_inventory_entry (id, name, description, price) values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180);
insert into plant_inventory_entry (id, name, description, price) values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200);
insert into plant_inventory_entry (id, name, description, price) values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300);
insert into plant_inventory_entry (id, name, description, price) values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400);
insert into plant_inventory_entry (id, name, description, price) values (12, 'Loader', 'Hewden Backhoe Loader', 200);
insert into plant_inventory_entry (id, name, description, price) values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250);
insert into plant_inventory_entry (id, name, description, price) values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300);

insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (1, 1, 'A01', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (2, 1, 'A02', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (3, 3, 'A03', 'UNSERVICEABLE_REPAIRABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (4, 2, 'A04', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (5, 2, 'A05', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (6, 2, 'A06', 'UNSERVICEABLE_CONDEMNED');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (7, 2, 'A07', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (8, 8, 'A08', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (9, 9, 'A09', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (10, 10, 'A10', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (11, 11, 'A11', 'UNSERVICEABLE_INCOMPLETE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)values (12, 12, 'A12', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (13, 13, 'A13', 'UNSERVICEABLE_CONDEMNED');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (14, 14, 'A14', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (15, 1, 'A15', 'SERVICEABLE');


insert into plant_reservation (id, plant_id, start_date, end_date) values (1, 2, '2017-03-22', '2017-03-24');
insert into plant_reservation (id, plant_id, start_date, end_date) values (2, 1, '2017-02-22', '2017-02-24');
insert into plant_reservation (id, plant_id, start_date, end_date) values (3, 15, '2017-04-22', '2017-04-24');
insert into plant_reservation (id, plant_id, start_date, end_date) values (4, 5, '2017-02-22', '2017-02-24');
insert into plant_reservation (id, plant_id, start_date, end_date) values (5, 15, '2017-02-20', '2017-02-22');
insert into plant_reservation (id, plant_id, start_date, end_date) values (6, 4, '2016-05-02', '2016-05-10');


insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (1,'2017-03-22', '2017-03-24','2015',1);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (2,'2017-03-22', '2017-03-24','2015',2);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (3,'2017-03-22', '2017-03-24','2015',3);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (4,'2017-03-22', '2017-03-24','2014',3);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (5,'2017-03-22', '2017-03-24','2013',4);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (6,'2017-03-22', '2017-03-24','2013',5);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (7,'2017-03-22', '2017-03-24','2012',6);
insert into maintenance_plan (id, end_date, start_date, year_of_action, plant_id) values (8,'2017-03-22', '2017-03-24','2011',6);

// reservation_id - constraint on id column from plant_reservation
//insert into maintenance_task (id, description, price,  end_date, start_date, type_of_work, reservation_id) values (1,'Tail light bulbs', 25, '2017-03-22', '2017-03-24','CORRECTIVE',1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (1,'Tail light bulbs', 25, 'CORRECTIVE', 1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (2,'Engine oil', 35, 'CORRECTIVE',1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (3,'Air filters', 45, 'CORRECTIVE',2);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (4,'Battery and cables', 95, 'CORRECTIVE',1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (5,'Bulbs and cables', 25, 'CORRECTIVE',1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (6,'Filters and oil', 75, 'CORRECTIVE',1);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (7,'Complete checkup', 135, 'CORRECTIVE',2);
insert into maintenance_task (id, description, price, type_of_work, reservation_id) values (8,'Tire checkup', 50, 'PREVENTIVE',2);

insert into maintenance_plan_task (maintenance_plan_id, task_id) values (1,1);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (2,2);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (3,3);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (4,4);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (5,5);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (6,6);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (7,7);
insert into maintenance_plan_task (maintenance_plan_id, task_id) values (8,8);



insert into guest(id, details, preferences, balance,transactionref) values (1,'qwerty', 'qwerty',25,'http://tinyurl.com/123');
insert into property_item (id, price, city, address, area, floor, amenities, services, start_date, end_date) values(1, 4,'Tartu','Lossi1',34,2,'no','no','2017-03-27','2017-03-24' );
insert into booking_order (id, status, property_id, request_id) values (1,'ACCEPTED',1,1);
insert into booking_request (id, bitcoins,rentamount,end_date, start_date, guest_id, item_id, order_id) values (1,3,6,'2017-03-27', '2017-03-24',1,1,1);
